# Neovim-java pages

Neovim-java logo is based on Neovim logo by [Jason Long](https://twitter.com/jasonlong), [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/). Modified logo is also licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/).
